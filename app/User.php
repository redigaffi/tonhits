<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected   $table      = 'users';
    public      $timestamps = false;

    public function getReferrer(){
        return $this->belongsTo('App\User', 'referredby', 'username')->first();
        //return $this->hasOne('App\Users', 'username', 'referredby');
    }

    public function getReferrals(){
        return $this->hasMany('App\User', 'referredby', 'username');
    }

    public function getPurchases(){
        return $this->hasMany('App\UserUplineComission', 'user_id', 'id')->get();
    }

    public function getAccountAccessLogs(){
        return $this->hasMany('App\AccountAccessLog', 'user_id', 'id')->get();
    }
    
    public function getTickets(){
        return $this->hasMany('App\SupportTicket', 'user_id', 'id')->get();
    }

    public function getMemberShipPurchases(){
        return $this->belongsTo('App\MembershipPurchase', 'membership_id', 'id')->first();
    }

    public function getWallet(){
        return $this->hasOne('App\Wallet', 'id', 'id')->first();
    }

    public function getEmailVerification(){
        return $this->hasOne('App\EmailVerify', 'user_id', 'id')->first();
    }

    public function getAdpacks(){
        return $this->hasMany('App\Adpacks', 'user_id', 'id')->get();
    }

    public function getUserBans(){
        return $this->hasMany('App\AccountsBanned', 'user_id', 'id')->get()->sortByDesc('date')->first();
    }
}
