<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App;
use App\Lang;
use App\Http\Requests;
use App\User;
use App\AccountAccessLog;
use App\EmailVerify;
use App\Wallet;
use App\BoardMembers;
use App\MembershipPurchase;


class SiteController extends Controller
{
    public function index(){
        return view('site/site');

    }

    public function login(Request $request){
        if ($request->getMethod() == 'POST'){

            $this->validate($request, [
                'email'         => 'required|exists:users|email',
                'password'      => 'required|min:6'
            ]);

            $user = User::where('email',    $request->email)
                        ->where('password', md5($request->password))
                        ->first();

            // User exists
            if($user != null){

                // Email address verified.
                if($user->getEmailVerification() == null){

                    // User is active.
                    if($user->active){
                        $request->session()->put('user', $user);

                        $user->last_login = date('Y-m-d H:i:s', strtotime('now'));
                        $user->save();

                        if(App::environment('production')){
                            $log   = addAccessLog($user->id, $request->ip());
                            $proxy = checkProxy($request->ip());

                            if($proxy){
                                Redirect::route('_login')
                                    ->send()
                                    ->with('message', 
                                        [
                                            'error', 
                                            \Lang::get('site.proxy_detected', [
                                                        'name'=>$user->name
                                                    ])
                                        ]);
                            }
                        }


                        Redirect::route('_me')
                            ->send();
                    }
                    // User is banned
                    else{
                        $ban = $user->getUserBans();

                        // Should not happen, but just in case.
                        if($ban->isEmpty()){
                            $msg = \Lang::get('site.error_weird');
                        }else{
                            $msg = \Lang::get('site.account_banned', ['from'=>$ban->from, 'to'=>$ban->to, 'reason'=>$ban->reason]);
                        }
                        Redirect::route('_login')->send()->with('message', ['error', $msg]);
                    }

                    // Email address not verified.
                }else{
                    exit(\Lang::get('site.not_verified'));

                    Redirect::route('_login')->send()->with('message', ['error', \Lang::get('site.not_verified')]);
                }

            }else{
                Redirect::route('_login')
                        ->send();
            }

        }else {
            return view('site/login');
        }

    }

    // Register
    /**
     * @param Request $request
     * @param string $referral
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request, $referral = 'system'){
        // Register user in myBB
        // md5(md5(Random 8 char). md5(password))
        // md5(md5('kSdadGwv'). md5('imtosexxy99') );
        if ($request->getMethod() == 'POST'){

            $this->validate($request, [
                'name'          => 'required|min:5|max:20',
                'username'      => 'required|min:5|max:20|unique:users',
                'email'         => 'required|unique:users|email',
                'password'      => 'required|min:6',
                'repeatpassword'=> 'required|same:password',
                'pin'           => 'required|digits:4|integer',
                'referredby'    => 'required|exists:users,username'
            ]);

            $wallet = new Wallet();
            $wallet->save();

            $membershipPurchase = new MembershipPurchase();
            $membershipPurchase->membership_id = 1;
            $membershipPurchase->save();

            $user = new User;
            $user->name       = $request->name;
            $user->username   = $request->username;
            $user->email      = $request->email;
            $user->password   = md5($request->password);
            $user->pin        = $request->pin;
            $user->referredby = $request->referredby;
            $user->wallet_id  = $wallet->id;
            $user->membership_id = $membershipPurchase->id;
            $user->save();

            /*$boardMember = new BoardMembers();
            $boardMember->usergroup = 2;
            $boardMember->username  = $request->username;
            $boardMember->salt      = str_random(8);
            $boardMember->password  = md5(md5($boardMember->salt) . md5($request->password));
            $boardMember->save();*/

            // Send email with code: here please ¬¬ .
            if(App::environment('production')){

                
                /*$verify = new EmailVerify();
                $verify->user_id = $user->id;
                $verify->date = date('Y-m-d H:i:s', strtotime('now'));
                $code         = md5($user->id . $verify->date);
                $verify->code = $code;
                $verify->save();*/

                $log   = addAccessLog($user->id, $request->ip());
                $proxy = checkProxy($request->ip());

                if($proxy){
                    Redirect::route('_login')->send()->with('message', ['error', \Lang::get('site.proxy_detected', ['name'=>$user->name])]);
                }
            }

            Redirect::route('_login')
                ->send()
                ->with(
                    ['message' =>
                        [
                            'success', 
                            \Lang::get('site.register_success')
                        ]]
                );

        }else{
            return view('site/register', [
                'referredby' => $referral
            ]);
        }
    }

    public function tos(Request $request){
        return view('site/tos');
    }
}

