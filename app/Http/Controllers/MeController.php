<?php

namespace App\Http\Controllers;

use App\TicketAnswers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\TicketCategory;
use App\SupportTicket;
use App\Memberships;
use App\UserUplineComission;
use Lang;

class MeController extends Controller
{

    public function index(Request $request){
        /*echo 'me ';
        dd($request->getSession());
        $user = $request->getSession()->get('user');
        dd($user->getAdpacks());
        die();*/
        
        $user = $request->getSession()->get('user');


        return view('me/index', [
            'user'      => $user,
            'wallet'    => $user->getWallet()
        ]);


    }

    public function accessLog(Request $request){
        $accessLogs = $request->getSession()->get('user')->getAccountAccessLogs();

        return view('me/accesslog', [
            'accessLogs' => $accessLogs
        ]);
    }

    public function upgrade(Request $request){
        $user    = $request->getSession()->get('user');
        $wallet  = $user->getWallet();

        if($request->getMethod() == 'POST'){
            $membershipId = $request->membership_id;
            $balance      = $request->balance;
            $membership   = Memberships::find($membershipId);

            // Does this membership exists (?)
            if($membership !== null){
                $wallet       = $user->getWallet();

                // Does this balance exists (?)
                if($wallet->{$balance} !== null){

                    // Have i enough money to buy this membership (?)
                    if($wallet->{$balance} >= $membership->price){

                        // Update Wallet
                        $wallet->{$balance} -= $membership->price;
                        $wallet->save();

                        // Update user membership purchase + purchase date
                        $membershipPurchase = $user->getMemberShipPurchases();
                        $membershipPurchase->membership_id = $membershipId;
                        $membershipPurchase->save();

                        // Add upline comission (depending on his membership)
                        addUplineComission($user, $membership->price, 'MEMBERSHIP');

                        Redirect::route('_upgrade')->send()->with('message', ['success',\Lang::get('me.membership_purchased')]);

                    }else{
                        Redirect::route('_upgrade')->send()->with('message', ['error', \Lang::get('me.membership_error')]);
                    }

                }else{
                    // hacking :?
                }
            }

        }else{
            return view('me/upgrade', [
                'memberships'   => Memberships::get(),
                'wallet'        => $wallet
            ]);
        }
        
    }

    public function myReferrals(Request $request){
        $user = $request
            ->getSession()
            ->get('user');

        return view('me/myreferrals', [
                'user' => $user
        ]);
    }

    public function buyAdPacks(Request $request){
        $user = $request
                       ->getSession()
                       ->get('user');


        return view('me/buyadpacks',[
            'user' => $user
        ]);
    }

    public function viewReferral(Request $request, $id){

        $referralPurchasesById = $request
            ->getSession()
            ->get('user')
            ->getReferrals()
            ->where('id', $id)
            ->first()
            ->getPurchases();

        return view('me/viewreferral', [
            'referral' => $referralPurchasesById
        ]);
    }

    public function myAdCampaigns(Request $request){
        //dd($request->getSession()->get('user'));
        return view('me/myadcampaigns');

    }

    public function logout(Request $request){
        $request->session()->flush();
        Redirect::route('_index')
            ->send();
    }

    public function tickets(Request $request){
        $user     = $request->getSession()->get('user');
        $tickets  = $user->getTickets();

        return view('me/mytickets', [
            'tickets' => $tickets
        ]);
    }

    public function viewTicket(Request $request, $id){
        $ticket         = SupportTicket::find($id);
        $msg            = $ticket->msg()->get();

        $myUser         = $request->getSession()->get('user')->id;

        // 0 because first msg is ever from the owner of the ticket.
        if($msg[0]->user()->first()->id != $myUser){
            echo 'not your ticket';
        }else{
            return view('me/viewticket', [
                'msg' => $msg
            ]);
        }

    }

    public function newTicket(Request $request){
        if($request->getMethod() == 'POST'){

            $support = new SupportTicket;
            $support->category =  $request->category;
            $support->user_id  =  $request->getSession()->get('user')->id;
            $support->save();

            $msg        = new TicketAnswers;
            $msg->user  = $request->getSession()->get('user')->id;
            $msg->msg   = $request->msg;

            $support->msg()->save($msg);

            Redirect::route('_mytickets')->send()->with('message', ['success', Lang::get('me.ticket_create_success')]);

        }else{
            $ticketCategories = TicketCategory::all()->all();
            $categoriesArray  = [];

            foreach ($ticketCategories as $categoryObject) {
                $categoriesArray[$categoryObject->id] = $categoryObject->type;
            }

            return view('me/newticket', [
                'categoriesArray' => $categoriesArray
            ]);
        }
    }


    public function settings(Request $request){

        $user = $request->getSession()->get('user');

        if($request->getMethod() == 'POST'){

            $this->validate($request, [
                'name'          => 'required|min:5|max:20',
                'password'      => 'required|min:6',
                'currentpass'   => 'required|same:password',
                'pin'           => 'required|digits:4|integer',
            ]);

            $password = md5($request->currentpass);

            if($user->password == $password && $user->pin == $request->pin){
                $user->password = md5($request->password);
                $user->save();
                $request->getSession()->set('user', $user);

                Redirect::route('_settings')->send()->with('message', ['success', Lang::get('me.settings_success')]);

            }else{
                Redirect::route('_settings')->send()->with('message', ['error', Lang::get('me.settings_error')]);
            }
        }else{

            return view('me/settings', [
                'name' => $user->name,
            ]);
        }

    }

    public function deposit(){
        return view('me/deposit');
    }

}
