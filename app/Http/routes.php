<?php
use Illuminate\Support\Facades\View;
use App\Http\Middleware\LoggedIn;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Site Routes [non login required]
Route::group(['middleware' => ['web']], function(){

    Route::get('/', ['uses' =>  'SiteController@index', 'as' => '_index']);

    Route::get('/register/{referral?}', ['uses'=>'SiteController@register', 'as'=>'_register']);
    Route::post('/register', ['before' => 'csrf', 'uses'=>'SiteController@register', 'as'=>'_register']);

    Route::get('/login', ['uses'=>'SiteController@login', 'as'=>'_login']);
    Route::post('/login', ['uses'=>'SiteController@login', 'as'=>'_login']);

    Route::get('/tos', ['uses'=>'SiteController@tos', 'as'=>'_tos']);

    Route::group(['middleware' => ['App\Http\Middleware\LoggedIn', 'App\Http\Middleware\RefreshUser']], function(){
        Route::get('/me', ['uses'=>'MeController@index', 'as'=>'_me']);

        Route::get('/me/buyadpacks', ['uses' => 'MeController@buyAdPacks', 'as'=>'_buyadpacks']);
        Route::get('/me/adcampaigns', ['uses' => 'MeController@myAdCampaigns', 'as'=>'_adcampaigns']);
        Route::get('/me/upgrade', ['uses'=>'MeController@upgrade', 'as'=>'_upgrade']);
        Route::post('/me/upgrade', ['uses'=>'MeController@upgrade', 'as'=>'_upgrade']);
        Route::get('/me/myreferrals', ['uses'=>'MeController@myReferrals', 'as'=>'_myreferrals']);
        Route::get('/me/viewreferral/{id}', ['uses'=>'MeController@viewReferral', 'as'=>'_viewreferral']);
        Route::get('/me/logout', ['uses' => 'MeController@logout', 'as' => '_logout']);

        Route::get('/me/accesslog', ['uses' => 'MeController@accessLog', 'as' => '_accesslog']);
        Route::get('/me/settings', ['uses' => 'MeController@settings', 'as' => '_settings']);
        Route::post('/me/settings', ['uses' => 'MeController@settings', 'as' => '_settings']);

        Route::get('/me/mytickets', ['uses' => 'MeController@tickets', 'as' => '_mytickets']);
        Route::get('/me/newticket', ['uses' => 'MeController@newTicket', 'as' => '_newticket']);
        Route::post('/me/newticket', ['uses' => 'MeController@newTicket', 'as' => '_newticket']);
        Route::get('/me/viewticket/{id}', ['uses'=>'MeController@viewTicket', 'as'=>'_viewticket']);

        Route::get('/me/deposit', ['uses'=>'MeController@deposit', 'as'=>'_deposit']);

    });
});
