<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class RefreshUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oldUser = $request->getSession()->get('user')->id;
        $newUser = User::where('id', $oldUser)->first();
        $request->session()->set('user', $newUser);

        return $next($request);
    }
}
