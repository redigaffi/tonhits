<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adpacks extends Model
{
    public      $timestamps = false;
    protected   $table      = 'adpacks';
    
}
