<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountAccessLog extends Model
{
    protected   $table      = 'account_access_log';
    public      $timestamps = false;
}
