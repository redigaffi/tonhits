<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketAnswers extends Model
{
    public      $timestamps = false;
    protected   $table      = 'ticket_answers';

    public function user(){
        return $this->belongsTo('App\User', 'user', 'id');
    }
}
