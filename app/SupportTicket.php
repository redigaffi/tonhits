<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    public      $timestamps = false;
    protected   $table = 'support_tickets';

    public function msg(){
        return $this->hasMany('App\TicketAnswers', 'id_ticket', 'id');
    }
}
