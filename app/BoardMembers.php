<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardMembers extends Model
{
    protected   $table      = 'mybb_users';
    public      $timestamps = false;
}
