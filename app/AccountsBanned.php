<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountsBanned extends Model
{
    protected   $table      = 'accounts_banned';
    public      $timestamps = false;
}
