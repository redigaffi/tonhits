<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCategory extends Model
{
    protected   $table      = 'ticket_category';
    public      $timestamps = false;
}
