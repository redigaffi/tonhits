<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipPurchase extends Model
{
    public      $timestamps = false;
    protected   $table      = 'membership_purchase';
    
    public function getMemberShip(){
        return $this->belongsTo('App\Memberships', 'membership_id', 'id')->first();
    }

}
