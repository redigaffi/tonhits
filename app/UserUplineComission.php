<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUplineComission extends Model
{
    protected   $table      = 'user_upline_comission';
    public      $timestamps = false;

}
