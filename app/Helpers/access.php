<?php
function addAccessLog($userId, $ip){

    $json   = json_decode(file_get_contents('http://ip-api.com/json/'.$ip), true);

    if($json !== null && $json['status'] != 'fail'){

        $acl    = new \App\AccountAccessLog;
        $acl->user_id = $userId;
        $acl->ip      = $ip;
        $acl->country = $json['country'];
        $acl->region  = $json['regionName'];
        $acl->city    = $json['city'];
        $acl->zip     = $json['zip'];
        $acl->lat     = $json['lat'];
        $acl->lon     = $json['lon'];
        $acl->isp     = $json['isp'];
        $acl->save();

        return true;

    }else{
        // something wierd.
        return false;
    }
}

function checkProxy($ip){
    $result = (float) file_get_contents("http://check.getipintel.net/check.php?ip=$ip&contact=jordixboy@gmail.com");

    if($result < 0.89){
        return false;
    }else{
        return true;
    }
}