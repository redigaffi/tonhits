<?php
function generateWalletDropDown(\App\Wallet $wallet){
    $processors = [
        'btc_balance'  => 'Bitcoin: ',
        'stp_balance'  => 'SolidTrustPay: ',
        'pm_balance'   => 'PerfectMoney: ',
        'main_balance' => 'Main Balance: '
    ];

    $string     = '<select name="balance" class="form-control input-lg">';
    $balances   = $wallet->toArray();

    foreach($balances as $balance => $money){
        if(isset($processors[$balance])){
            $string .= '<option value="'.$balance.'">'.$processors[$balance].'$'.$money.'</option>';
        }
    }

    $string .= '</select>';

    return $string;
}

function addUplineComission(\App\User $user, $price, $type){
    $upline                      = $user->getReferrer();
    $uplineMemberShip            = $upline->getMemberShipPurchases()->getMemberShip();
    $uplineWallet                = $upline->getWallet();
    $refComission                = $price*$uplineMemberShip->refcomission;
    $uplineWallet->main_balance += $refComission;
    $uplineWallet->save();

    // Add log for upline
    $uplineLog = new \App\UserUplineComission();
    $uplineLog->user_id = $user->id;
    $uplineLog->price   = $price;
    $uplineLog->upline  = $refComission;
    $uplineLog->type    = $type;
    $uplineLog->save();

    return true;
}