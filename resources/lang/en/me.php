<?php
return [
    /**
    |--------------------------------------------------------------------------
    | Me language file
    |--------------------------------------------------------------------------
    |  Language Strings for the general me (logged in).
    |
     */

    // Errors
    'membership_error'              =>     'You have no money to purchase this membership',
    'settings_error'                =>     'Your password, or pin are incorrect.',

    // Success
    'settings_success'              =>      'Your account has succesfully changed.',

    // Success
    'membership_purchased'          =>      'Membership Purchased',

    // Table Fields
    'table_date'                    =>      'Date',
    'table_country'                 =>      'Country',
    'table_ip'                      =>      'IP',
    'table_name'                    =>      'Name',
    'table_username'                =>      'Username',
    'table_membership'              =>      'Membership',
    'table_email'                   =>      'Email',
    'table_advcredits'              =>      'Advertising Credits',
    'table_balance'                 =>      'Balance',
    'table_actadpacks'              =>      'Active Adpacks',
    'table_adsviewtoday'            =>      'Ads Viewed Today',
    'table_minadsqualify'           =>      'Ads needed to qualify',
    'table_referrals'               =>      'Referrals',
    'table_refcom'                  =>      'Referral Commission',
    'table_totalrefcom'             =>      'Total Commission Earned',
    'table_membersince'             =>      'Member Since',
    'table_actions'                 =>      'Actions',
    'table_moredetails'             =>      'More Details',
    'table_empty'                   =>      'Nothing to show.',
    'table_category'                =>      'Category',
    'table_status'                  =>      'Status',
    'table_creationdate'            =>      'Creation Date',
    'table_lastmsg'                 =>      'Last message',
    'table_viewmsg'                 =>      'View Message',
    'table_open'                    =>      'Open',
    'table_close'                   =>      'Close',
    'table_readmsg'                 =>      'Read Messages',
    'table_free'                    =>      'FREE',
    'table_adpack'                  =>      'AdPack',
    'table_profit'                  =>      'Profit',
    'table_max_ad'                  =>      'Max AdPacks',
    'table_min_ads_val'             =>      'Minimum ads to validate',
    'table_vac_days'                =>      'Vacation days',
    'table_max_adp_ret'             =>      'Max Daily AdPack Return',
    'table_referral_com'            =>      'Referral comission purchases/ads',
    'table_ref_type'                =>      'Type',
    'table_org_am'                  =>      'Original Amount',
    'table_com_earn'                =>      'Comissions Earned',

    // Titles
    'title_my'                      =>      'Me',
    'accesslog'                     =>      'Your access history',
    'buyadpacks'                    =>      'Buy Adpacks to advertise you',
    'myadcampaigns'                 =>      'My adpack overwiev',
    'myreferrals'                   =>      'Your referrals overview',
    'mytickets'                     =>      'My support tickets',
    'newticket'                     =>      'Send a support ticket',
    'viewreferral'                  =>      'Your referrals overview',
    'upgrade'                       =>      'Upgrade your account',
    'viewticket'                    =>      'Viewing tickets',
    'ticket_create_success'         =>      'A new ticket has been opened.',
    'my_ref_link'                   =>      'My advertising link',

    // Description (Content)
    'desc_buyadpack'                =>      'Adpacks are packs that gives you credits, with credits you can buy adcampaigns and advertise your website.',
    'desc_myadcampaign'             =>      'Here you can see your purchased adpacks, so you can have a detailed tracing.',

    // Empty
    'empty_referrals'               =>      'You have no referrals',
    'empty_mytickets'               =>      'You have no tickets',

    // Buttons
    'send_ticket'                   =>      'Send support ticket',
    'purchase_membership'           =>      'Purchase',


    // Texts
    'purch_bal'                     =>      'Purchase Balance',

    // Inputs
    'input_name'                    =>       'Name',
    'input_username'                =>       'Username',
    'input_email'                   =>       'Email',
    'input_pin'                     =>       'Pin',
    'input_pass'                    =>       'Password',
    'input_repass'                  =>       'Repeat Password',

    // Site texts
    'site_h2'                       =>        'Welcome to TonHits!',
    'site_p'                        =>        'Your favorite investment site.',
    'site_AdPacks'                  =>        'Ad Packs',
    'site_AdPacks_text'             =>        'We offer up to X% ROI on your advertising package purchases.',
    'site_Safeinv'                  =>        'Safe investment',
    'site_Safeinv_text'             =>        'We are in constant development of new products which help us deduct the debt.',
    'site_button'                   =>        'Register Now!',
    'site_copyright'                =>        'TonHits. All rights reserved.',

];