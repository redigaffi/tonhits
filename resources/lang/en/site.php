<?php


return [
    /**
    |--------------------------------------------------------------------------
    | Site language file
    |--------------------------------------------------------------------------
    |  Language Strings for the general site (not logged in).
    |
    */

    // Proxy
    'proxy_detected' => 'Proxy detected, your account :name will be permanently banned.',


    // Errors
    'error_weird'    => 'Something weird happened, please contact us.',

    // Banned
    'account_banned' => 'From :from to :to your account is banned, reason: :reason',

    // Email
    'not_verified'   => 'Your email address isn\'t verified, please check your inbox.',

    // Success
    'register_success' => 'You have registered your account, please check your email account to verify your account',
];


?>