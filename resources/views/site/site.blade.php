<!DOCTYPE HTML>
<!--
	Iconic by Pixelarity
	pixelarity.com @pixelarity
        License: pixelarity.com/license
    -->
<html>
<head>
        <title>TonHits</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/table.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/tablesaw.css') }}" />
        <link rel="stylesheet" href="{{ asset('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css') }}" />
</head>
<body class="homepage">
<div id="page-wrapper">

        <!-- Header Wrapper -->
        <div id="header-wrapper">

                <!-- Header -->
                <div id="header" class="container">

                        <!-- Logo -->
                                <h1><a href="index.html" id="logo">TonHits</a></h1>
                        <!-- Nav -->
                        @include('nav/nav')

                </div>

        </div>

        <!-- Main Wrapper -->
        <div id="main-wrapper">

                <!-- Main -->
                <div id="banner-wrapper">
                        <section id="banner" class="container">
                                <div class="row">
                                        <div id="content" class="12u">

                                                <header class="major">
                                                        <h2>{{ trans('me.site_h2') }}</h2>
                                                        <p>{{ trans('me.site_p') }}</p>
                                                </header>

                                                <ul class="pennants">
                                                        <li>
                                                                <a href="#" class="pennant"><span class="fa fa-desktop"></span></a>
                                                                <header>
                                                                        <h3>{{ trans('me.site_AdPacks') }}</h3>
                                                                </header>
                                                                <p>{{ trans('me.site_AdPacks_text') }}</p>
                                                        </li>
                                                        <li>
                                                                <a href="#" class="pennant"><span class="icon fa-cog"></span></a>
                                                                <header>
                                                                        <h3>{{ trans('me.site_Safeinv') }}</h3>
                                                                </header>
                                                                <p>{{ trans('me.site_Safeinv_text') }}</p>
                                                        </li>
                                                </ul>

                                                <footer>
                                                        <a href="{{ route('_register') }}" class="button big">{{ trans('me.site_button') }}</a>
                                                </footer>

                                        </div>
                                </div>
                        </section>
                </div>

        </div>





        <!-- Copyright -->
        <div id="copyright">
                &copy; {{ trans('me.site_copyright') }}
        </div>

</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.dropotron.min.js') }}"></script>
<script src="{{ asset('js/skel.min.js') }}"></script>
<script src="{{ asset('js/util.js') }}"></script>
<script src="{{ asset('js/tablesaw.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js') }}"></script>

</body>
</html>