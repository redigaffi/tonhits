@extends('base')

@section('titleBig',    'Register')
@section('titleSmall',  'Register an account for free')

@section('content')
    {{ Form::open(['url' => route('_register')]) }}
        {{ Form::token() }}

        <strong>Name: </strong>
        {{  Form::text('name', null, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('name')) > 0 )
            <div class="errorForm">
                <li>
                @foreach($errors->get('name') as $error)
                    <ul>{{ $error }}</ul>
                @endforeach
                </li>
            </div>
        @endif

        <strong>Username: </strong>
        {{  Form::text('username', null, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('username')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('username') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        <strong>Email: </strong>
        {{  Form::text('email', null, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('email')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('email') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        <strong>Pin (4 Digits): </strong>
        {{  Form::number('pin', null, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('pin')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('pin') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        <strong>Password: </strong>
        {{  Form::password('password', ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('password')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('password') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        <strong>Repeat Password: </strong>
        {{  Form::password('repeatpassword', ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('repeatpassword')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('repeatpassword') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        {{  Form::hidden('referredby', $referredby)  }}

        {{  Form::hidden('_token', csrf_token())  }}

        {{ Form::submit('Register an account', ['class'=>'btn btn-lg btn-success']) }}
    {{ Form::close() }}
@endsection