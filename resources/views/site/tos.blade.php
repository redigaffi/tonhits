@extends('base')

@section('titleBig',    'Information')
@section('titleSmall',  'Terms of service')

@section('content')
        <!-- Main Wrapper -->
">
                    <h2 class="nomargin">My account:</h2>
                    <div class="accordion">
                        <h3>Can I change my username?</h3>
                        <div>
                            <p>No, you can't change your username once you have chosen.</p>
                        </div>

                        <h3>Can I change my real name?</h3>
                        <div>
                            <p>No, It’s not allowed to change your personal data.</p>
                        </div>

                        <h3>Can I delete my account?</h3>
                        <div>
                            <p>Yes, but It’s not possible to delete it manually, to do it, you must leave your account inactive 3 months, then it will be deleted automatically from the system.</p>
                        </div>
                    </div>
                    <br>
                    <h2 class="nomargin">AdPacks:</h2>
                    <div class="accordion">
                        <h3>How much costs an Ad Pack?</h3>
                        <div>
                            <p>An Ad Pack costs $25 plus fees, they depend the payment processor you use to buy it, there are no fees if you buy with your account balance.</p>
                        </div>

                        <h3>What do I earn by purchasing an Ad Pack?</h3>
                        <div>
                            <p>You will get 1000 credits of Traffic Exchange, 500 banner credits, and you will get your investment back with the earnings up to 120% in 50 days.</p>
                        </div>

                        <h3>How much can I earn at day?</h3>
                        <div>
                            <p>We give you $0.5 fixed per day, plus a percentage of earnings depending on the daily revenues.</p>
                        </div>

                        <h3>Why do I have to purchase Ad Packs?</h3>
                        <div>
                            <p>Buy an Ad Pack It’s a secure way to win traffic totally free with a refund of your investment up to 120% depending on the daily revenues, ensuring your initial investment in 50 days.</p>
                        </div>

                        <h3>What happens if I don’t see the daily surf ads?</h3>
                        <div>
                            <p>You have to watch daily the 10 surf ads to collect the profits of the Ad Packs, if for example today you don’t watch them, tomorrow you will not receive the $0.5 fixed plus the respective gains.</p>
                            <p>In the case that you had to travel, or something else, we have a vacations mode that you can buy, with two differents options, one that it hold for one week and costs $2, and other that hold for one month and costs $8, and during them, you will not receive the daily $0.5, but you will not lose the corresponding Ad Packs also. Once you are back, they will continue where you had left them.</p>
                        </div>

                        <h3>What if I want deactivate the vacations mode and watch the ads?</h3>
                        <div>
                            <p>You can disable the vacation mode, but bear in mind that can’t be combined, when you deactivate it, if onwards you want to continue without seeing ads, you must purchase another.</p>
                        </div>
                    </div>
                    <br>
                    <h2 class="nomargin">Referrals:</h2>
                    <div class="accordion">
                        <h3>Where are my referral link?</h3>
                        <div>
                            <p>Your referral link is below your Country on your profile.</p>
                            <p>In order to get referrals you simply have to copy it and share it wherever you want.</p>
                        </div>

                        <h3>How much do I earn for referral?</h3>
                        <div>
                            <p>There is one level of referrals, and you earn the 10% of the buys done on each Ad Pack.</p>
                        </div>

                        <h3>Can I change my sponsor?</h3>
                        <div>
                            <p>No, you can’t change the sponsor.</p>
                            <p>The only way to change it, would be if you comply certain requirements, including:</p>
                            <ul class="acclist">
                                <li>The registration of your account is less than one month old.</li>
                                <li>Create a new account, must notify with a ticket explaining your reasons, within a maximum period of 7 days. I.e, if in a maximum of 7 days have not given explanations of the reason why you has made a new account, will not be possible to make the proper balance transfer, Ad Packs and / or actions.</li>
                                <li>Giving us all the necessary information regarding your account to confirm that you are the owner.</li>
                                <li>Once this is done, we will proceed to transfer the balance from your old account to the new one together with the Ad Packs and the actions that would have bought.</li>
                            </ul>
                        </div>

                        <h3>I have lost referrals</h3>
                        <div>
                            <p>The users with more than two months offline, will have a deactivation of their accounts, and will also be deleted temporarily from your total referrals until they re-activate their account, or permanently if they haven’t re-activated in 3 months.</p>
                        </div>
                    </div>
                    <br>
                    <h2 class="nomargin">Payments:</h2>
                    <div class="accordion">
                        <h3>How long will take my withdrawal?</h3>
                        <div>
                            <p>The withdrawal is going to be received depending the method you choose.</p>
                            <ul class="acclist">
                                <li><b>Standard Method:</b> The money will be sent to your account within a maximum period of 7 days without any extra costs, the waiting time will depend on the date of the position that you are on the list of withdrawals.</li>
                                <li><b>Urgent Method:</b> Your withdrawal will be placed on a preferred list of withdrawals, and the money will be sent in your account within a maximum period of one to two days, with an extra cost of 2% of your total withdrawal.</li>
                            </ul>
                        </div>

                        <h3>What happens if I cancel my withdrawal?</h3>
                        <div>
                            <p>If you cancel your withdrawal, in the case of having made a standard withdrawal, the fees are taken away depending on the payment processor that you have requested the withdrawal, and It’s canceled instantly. And in the case of having made an urgent withdrawal, It’s the same as the standard but in this one, you will have taken plus the fees for choose the urgent service.</p>
                        </div>
                    </div>
                    <br>
                    <h2 class="nomargin">Shareholder:</h2>
                    <div class="accordion">
                        <h3>What do I earn by purchasing actions?</h3>
                        <div>
                            <p>For every dollar invested, $0.1, which is the 10%, it’s addressed to the monthly dividends.</p>
                            <p>In the system there are currently a total of 10 million shares, that means that for every action you earn 0.0001 $ monthly, and it helps the system to keep going just by keeping these shares on your balance sheet, or you can just wait until the price raise and then sell them, earning profits.</p>
                        </div>

                        <h3>What are the bids?</h3>
                        <div>
                            <p>If you are thinking about buying shares, but you think that the current price is too high, you have this option to make an offer to purchase at the price you wish, and if it drops to that price, your offer will be in the first places or the first place in the purchase, and will be the first or among the first people to buy at the desired price.</p>
                            <p>Else, if you're thinking about selling shares, you can do the same with a bid, but instead of purchase, on the sale of shares, and if the price goes up to the price range you wish, you will be in the first places or in the first place of the desired sale price. But remember that when you put a bid on sale, we subtract you the fees directly, there’s no going back!, so if the bid is canceled, the fees are not returned.</p>
                        </div>

                        <h3>What is the value of a share?</h3>
                        <div>
                            <p>The shares haven’t a fixed price, this is given in two ways:</p>
                            <ul class="acclist">
                                <li>Once buyers and sellers have left their bids in the market and therefore have clear at what price they have interest in selling or buying.</li>
                                <li>The dividends, depending on what they are generating monthly, thanks to the games, and all that is generating money, and consequently reducing the debt of the system, they’ll be spread more or less, and therefore the share price, will rise or fall.</li>
                            </ul>
                        </div>

@endsection