@extends('base')

@section('titleBig',    'Login')
@section('titleSmall',  'Access your account')

@section('content')
    {{ Form::open(['url' => route('_login')]) }}
    {{ Form::token() }}

    <strong>Email: </strong>
    {{  Form::text('email', null, ['class'=>'form-control input-lg']) }}
    @if( count($errors->get('email')) > 0 )
        <div class="errorForm">
            <ul>
                @foreach($errors->get('email') as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <strong>Password: </strong>
    {{  Form::password('password', ['class'=>'form-control input-lg']) }}
    @if( count($errors->get('password')) > 0 )
        <div class="errorForm">
            <ul>
                @foreach($errors->get('password') as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{  Form::hidden('_token', csrf_token())  }}

    {{ Form::submit('Access Account', ['class'=>'btn btn-lg btn-success']) }}
    {{ Form::close() }}
@endsection