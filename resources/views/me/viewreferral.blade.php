@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.viewreferral'))

@section('content')
    <table id="referrals" class="table table-hover table-bordered">

        <thead>
            <th>{{ trans('me.table_ref_type') }}</th>
            <th>{{ trans('me.table_date') }}</th>
            <th>{{ trans('me.table_org_am') }}</th>
            <th>{{ trans('me.table_com_earn') }}</th>
        </thead>

        <tbody>
            @foreach($referral as $purchase)
                <tr>
                    <td>{{ $purchase->type }}</td>
                    <td>{{ date('Y-m-d', strtotime($purchase->date)) }}</td>
                    <td>${{ $purchase->price }}</td>
                    <td>${{  $purchase->upline }}</td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#referrals').DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                "info" : false,
                "iDisplayLength": 10,
                "columnDefs": [
                    { "orderable": true }
                ]
            });
        });
    </script>
@endsection

