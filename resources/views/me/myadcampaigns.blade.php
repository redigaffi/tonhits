@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.myadcampaigns'))


@section('content')

    {{ trans('desc_myadcampaign') }}

    <table id="adpacks"  class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Test</th>
            <th>Test</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Data</td>
            <td>Data 2</td>
        </tr>
        </tbody>

    </table>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#adpacks').DataTable();
    });

</script>
@endsection