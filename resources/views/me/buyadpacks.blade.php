@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.buyadpacks'))

@section('content')
    {{ trans('desc_buyadpack') }}
    {{ Form::open(['url' => route('_buyadpacks')]) }}
        {{ Form::token() }}


        {{  Form::hidden('_token', csrf_token())  }}

        {{ Form::submit('Buy adpack', ['class'=>'btn btn-lg btn-success']) }}
    {{ Form::close() }}

@endsection

