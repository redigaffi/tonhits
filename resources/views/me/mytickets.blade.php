@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.mytickets'))


@section('content')
    <table id="tickets"  class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>{{ trans('me.table_category') }}</th>
                <th>{{ trans('me.table_status') }}</th>
                <th>{{ trans('me.table_creationdate') }}</th>
                <th>{{ trans('me.table_lastmsg') }}</th>
                <th>{{ trans('me.table_viewmsg') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tickets as $ticket)
            <tr>
                <td> {{ $ticket['category'] }}</td>
                <td>
                    @if($ticket['active'] == 1)
                        <span style="color: green;">{{ trans('me.table_open') }}</span>
                    @else
                        <span style="color: red;">{{ trans('me.table_close') }}</span>
                    @endif
                </td>
                <td> {{ $ticket['date'] }} </td>
                <td> {{ $ticket->msg()->get()->last()->user()->first()->name }} </td>
                <td> <a href="{{ route('_viewticket', ['id'=>$ticket->id]) }}">{{ trans('me.table_readmsg') }}</a> </td>

            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#tickets').DataTable({
                "sEmptyTable": "{{ trans('me.empty_mytickets') }}}",
                "bFilter" : false,
                "bLengthChange": false,
                "info" : false,
                "iDisplayLength": 10
                
            });
        });
    </script>
@endsection