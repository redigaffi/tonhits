@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.myreferrals'))

@section('content')
    <table id="referrals" class="table table-hover table-bordered">

        <thead>
            <th>{{ trans('me.table_username') }}</th>
            <th>{{ trans('me.table_membersince') }}</th>
            <th>{{ trans('me.table_email') }}</th>
            <th>{{ trans('me.table_totalrefcom') }}</th>
            <th>{{ trans('me.table_actions') }}</th>
        </thead>

        <tbody>
            @foreach($user->getReferrals()->get() as $referral)
                <tr>
                    <td>{{ $referral->name }}</td>
                    <td>{{ date('Y-m-d', strtotime($referral->datejoined)) }}</td>
                    <td>{{ $referral->email }}</td>
                    <td>${{ $referral->getPurchases()->sum('upline') }}</td>
                    <td>
                        @if($referral->getPurchases()->sum('upline') > 0)
                            <a href="{{ route('_viewreferral', ['id'=>$referral->id]) }}"><button class="btn btn-primary">{{ trans('me.table_moredetails') }}</button></a>
                        @else
                            {{ trans('me.table_empty') }}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#referrals').DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                "info" : false,
                "iDisplayLength": 10,
                "language": {
                    "emptyTable": "{{ trans('me.empty_referrals') }}"
                }
            });
        });
    </script>
@endsection

