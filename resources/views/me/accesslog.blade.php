@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.accesslog'))

@section('content')
    <table id="referrals" class="table table-hover table-bordered">

        <thead>
            <th>{{ trans('me.table_date') }}</th>
            <th>{{ trans('me.table_country') }}</th>
            <th>{{ trans('me.table_ip') }}</th>
        </thead>

        <tbody>
            @foreach($accessLogs as $access)
                <tr>
                    <td>{{ date('d-m-Y', strtotime($access->date)) }}</td>
                    <td>{{ $access->country }}</td>
                    <td>{{ $access->ip }}</td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#referrals').DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                "info" : false,
                "iDisplayLength": 10,
                "language": {
                    "emptyTable": "{{ trans('me.empty_referrals') }}"
                }
            });
        });
    </script>
@endsection
