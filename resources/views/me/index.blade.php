@extends('base')

@section('titleBig',    'Me')
@section('titleSmall',  'Your account overview')

@section('content')
    <table class="table table-hover  table-bordered">

        <thead>

        </thead>

        <tbody>
           <tr>
               <th>{{ trans('me.table_name') }}</th>
               <td>{{ $user->name }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_username') }}</th>
               <td>{{ $user->username }}</td>
           </tr>
           <tr>
               <th>{{ trans('me.table_membership') }}</th>
               <td>
                   <span style="color: {{ $user->getMembershipPurchases()->getMembership()->color}}">{{ $user->getMembershipPurchases()->getMembership()->name }}</span>
               </td>
           </tr>

           <tr>
               <th>{{ trans('me.table_email') }}</th>
               <td>{{ $user->email }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_advcredits') }}</th>
               <td>{{ $user->credits }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_balance') }}</th>
               <td>${{ $wallet->pm_balance + $wallet->stp_balance + $wallet->btc_balance + $wallet->main_balance}}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_actadpacks') }}</th>
               <td>{{ $user->getAdpacks()->count() }}</td>
           </tr>

           <tr>
               @if($user->ads_viewed < $user->getMembershipPurchases()->getMembership()->min_ads_view)
                   <th><span style="color: red;"> {{ trans('me.table_adsviewtoday') }}</span></th>
               @else
                   <th> {{ trans('me.table_adsviewtoday') }}</th>
               @endif
               <td>{{ $user->ads_viewed }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_minadsqualify') }}</th>
               <td>{{ $user->getMembershipPurchases()->getMembership()->min_ads_view }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_referrals') }}</th>
               <td>{{ $user->getReferrals()->count() }}</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_refcom') }}</th>
               <td>{{ ($user->getMembershipPurchases()->getMembership()->refcomission*100) }}%</td>
           </tr>

           <tr>
               <th>{{ trans('me.table_totalrefcom') }}</th>
               <td>0</td>
           </tr>

           <tr>
               <th>{{ trans('me.my_ref_link') }}</th>
               <td>{{ route('_register') . '/' . $user->username }}</td>
           </tr>



        </tbody>

    </table>
@endsection