@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.upgrade'))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
@endsection

@section('content')

    {{ Form::open(['url' => route('_upgrade')]) }}
        {{ Form::token() }}
        {{  Form::hidden('_token', csrf_token())  }}

        <ul class="cd-pricing-list">
            @foreach($memberships as $membership)
                <li>
                    <header class="cd-pricing-header">
                        <h2>{{ $membership['name'] }}</h2>

                        <div class="cd-price">
                            <img src="{{ asset($membership['image']) }}">
                            <span class="cd-value">
                                @if($membership['price'] == 0)
                                    {{ trans('me.table_free') }}
                                @else
                                    ${{ $membership['price'] }}
                                @endif
                            </span>
                        </div>
                    </header> <!-- .cd-pricing-header -->

                    <div class="cd-pricing-body">
                        <ul class="cd-pricing-features">
                            <li><em>${{ $membership['adpack_price'] }} {{ trans('me.table_adpack') }}</em> </li>
                            <li><em>{{ 100+($membership['dailypercent']*$membership['daysexpireadpack']) }}%</em> {{ trans('me.table_profit') }} </li>
                            <li><em>{{ $membership['maxadpacks'] }}</em> {{ trans('me.table_max_ad') }}</li>
                            <li><em>{{ $membership['min_ads_view'] }}</em> {{ trans('me.table_min_ads_val') }}</li>
                            <li><em>{{ $membership['daysvacation'] }}</em> {{ trans('me.table_vac_days') }}</li>
                            <li><em>{{ $membership['dailypercent'] }}%</em> {{ trans('me.table_max_adp_ret') }}</li>
                            <li><em>{{ ($membership['refcomission']*100) }}%</em> {{ trans('me.table_referral_com') }}</li>
                        </ul>
                    </div> <!-- .cd-pricing-body -->

                    @if($membership['price'] > 0)
                        <footer class="cd-pricing-footer">
                            <button class="cd-select" name="membership_id" value="{{ $membership['id'] }}">{{ trans('me.purchase_membership') }}</button>
                        </footer> <!-- .cd-pricing-footer -->
                    @endif
                    
                </li>
            @endforeach
           
        </ul> <!-- .cd-pricing-list -->
        <br />
        <strong>{{ trans('me.purch_bal') }}: </strong>
        <?= generateWalletDropDown($wallet) ?>
    {{ Form::close() }}
@endsection



