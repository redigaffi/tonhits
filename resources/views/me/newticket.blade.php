@extends('base')

@section('titleBig',    trans('me.title_my'))
@section('titleSmall',  trans('me.newticket'))


@section('content')
    {{ Form::open(['url' => route('_newticket')]) }}
        {{ Form::token() }}

        <strong>Category: </strong>
        {{ Form::select('category', $categoriesArray, 'S', ['class' =>'form-control input-lg']) }}

        <strong>Message: </strong>
        {{ Form::textarea('msg', null, ['class'=>'form-control input-lg']) }}

        {{  Form::hidden('_token', csrf_token())  }}

        {{ Form::submit(trans('me.send_ticket'), ['class'=>'btn btn-lg btn-success']) }}
    {{ Form::close() }}
@endsection
