@extends('base')

@section('titleBig',    'Register')
@section('titleSmall',  'Register an account for free')

@section('content')
    {{ Form::open(['url' => route('_settings')]) }}
        {{ Form::token() }}

        <strong>{{ trans('me.input_name') }} </strong>
        {{  Form::text('name', $name, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('name')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('name') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif


        <strong>{{ trans('me.input_pass') }} </strong>
        {{  Form::password('password', ['class'=>'form-control input-lg'], 'aa') }}
        @if( count($errors->get('password')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('password') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif

        <strong>Current Password </strong>
        {{  Form::password('currentpass', ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('currentpass')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('currentpass') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif


        <strong>Current Pin </strong>
        {{  Form::text('pin', null, ['class'=>'form-control input-lg']) }}
        @if( count($errors->get('pin')) > 0 )
            <div class="errorForm">
                <li>
                    @foreach($errors->get('pin') as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                </li>
            </div>
        @endif
        {{  Form::hidden('_token', csrf_token())  }}

        {{ Form::submit('Update Information', ['class'=>'btn btn-lg btn-success']) }}
    {{ Form::close() }}
@endsection