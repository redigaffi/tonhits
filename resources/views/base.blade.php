
<!DOCTYPE HTML>
<html>
<head>
    <title>Tonhits</title>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/table.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/tablesaw.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.structure.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.theme.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css') }}" />

    @yield('css')


</head>
<body class="no-sidebar">
<div id="page-wrapper">

    <!-- Header Wrapper -->
    <div id="header-wrapper">

        <!-- Header -->
        <div id="header" class="container">

            <!-- Logo -->
            <h1><a id="logo">Tonhits</a></h1>

            <!-- Nav -->
            @include('nav/nav')

        </div>

    </div>

    <!-- Main Wrapper -->
    <div id="main-wrapper">

        <!-- Main -->
        <div id="page" class="container">

            <!-- Main Heading -->
            <div class="title-heading">
                <h2>@yield('titleBig')</h2>
                <p> @yield('titleSmall')</p>
            </div>

            <!-- Main Content -->
            <div id="main">

                @if(Session::has('message'))
                <div id="alertmsg" class="alertmsg-{{ Session::get('message')[0] }}">
                    {{ Session::get('message')[1] }}
                </div>
                @endif
                <div class="row">

                    <div id="content" class="12u">
                        <!-- <header>
                            <h2>Nunc fringilla dis natoque amet gravida turpis</h2>
                        </header> -->
                        @section('content')
                        @show
                    </div>
                </div>
            </div>
            <!-- Main Content -->

        </div>
        <!-- Main -->

    </div>



    <!-- Copyright -->
    <div id="copyright">
        Tonhits. All rights reserved
    </div>

</div>
<!-- Scripts -->


<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('http://code.jquery.com/ui/1.11.4/jquery-ui.js') }}"></script>
<script src="{{ asset('js/jquery.dropotron.min.js') }}"></script>
<script src="{{ asset('js/skel.min.js') }}"></script>
<script src="{{ asset('js/util.js') }}"></script>
<script src="{{ asset('js/tablesaw.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js') }}"></script>

@yield('js')


</body>
</html>
