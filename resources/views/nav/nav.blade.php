<nav id="nav">
    <ul>
         @if(!Session::has('user'))

            <li><a href="{{ route('_index') }}">Site</a></li>
            <li><a href="{{ route('_login') }}">Login</a></li>
            <li><a href="{{ route('_register') }}">Register</a></li>
            <li><a href="{{ route('_tos') }}">Tos</a></li>
            <li><a href="index.html">Forum</a></li>

        @else
            
            @if(Session::get('user')->role == 3)
                <span style="color: red">
                    <strong>ADMIN</strong>
                </span>
            @elseif(Session::get('user')->role == 2)
                <span style="color: blue">
                    <strong>MOD</strong>
                </span>
            @else
                <span style="color: {{ Session::get('user')->getMemberShipPurchases()->getMemberShip()->color }}">
                    <strong>{{ Session::get('user')->getMemberShipPurchases()->getMemberShip()->name }}</strong>
                </span>
            @endif
            <li>
                <a href="#">Support</a>
                <ul>
                    <li><a href="{{ route('_mytickets') }}">My Tickets</a></li>
                    <li><a href="{{ route('_newticket') }}">New Ticket</a></li>
                </ul>
            </li>
            <li>
                 <a href="#">Account</a>
                 <ul>
                    <li><a href="{{ route('_me') }}">Account summary</a></li>
                     <li><a href="{{ route('_myreferrals') }}">Referral overview</a></li>
                     <li><a href="{{ route('_upgrade') }}">Upgrade Account</a></li>
                     <li><a href="{{ route('_accesslog') }}">Access History</a></li>
                     <li><a href="{{ route('_settings') }}">Account Settings</a></li>
                    <li><a href="{{ route('_logout') }}">Logout</a></li>

                </ul>
            </li>
             <li>
                <a href="{{ route('_deposit') }}"><span style="color: green;">Deposit Funds</span></a>
             </li>
        @endif


    </ul>
</nav>

