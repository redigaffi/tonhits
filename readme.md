# Tonhits. #

**[!] Requirements:**

- Apache (>=) 2.4
- PHP 	 (>=) 7.00
- PHP-EXT: php7.0  php7.0-dom php7.0-curl php7.0-mcrypt php7.0-mbstring php7.0-xml php7.0-json php7.0-mysql libapache2-mod-php7.0

**[!] Install Composer**

- curl -sS https://getcomposer.org/installer | php
- sudo mv composer.phar /usr/local/bin/composer

**[!] Permissions**

- sudo chmod -R 777 tonhits/ (Yes, forgive us about that ...)

**[!] Apache2 Modules**

You have to enable mod rewrite for apache2 with the following command:

- sudo a2enmod rewrite

**[!] Virtualhost setup** 

Create a new virtualhost with the following content and change it to your needs:


```
#!python

<VirtualHost *:80>
	#ServerName tonhits.com

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/tonhits/public

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	<Directory /var/www/html/tonhits/>
	        AllowOverride All
	</Directory>
</VirtualHost>

```

**[!] Defining Environment**

In the root of the project create a file called .env with the following content:

```
#!python

APP_ENV=local
APP_DEBUG=true
APP_KEY=cC6HLBneofRR6Htd86Uu6pkOI7BykVif
APP_URL=http://198.27.65.48

DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tonhits
DB_USERNAME=root
DB_PASSWORD=imtosexxy99

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

```
Change your database settings and the url of the project.

**[!] Install Laravel plugins with Composer:**

In the root of the project execute the following commands:

- php composer install

**[!] Generate application secret key**

In the root of the project execute the following commands:

- php artisan key:generate