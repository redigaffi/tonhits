<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('memberships')->insert(
        [
            'id'                    => 1,
            'name'                  =>  'Standard',
            'maxadpacks'            => 100,
            'refcomission'          => 0.05,
            'dailypercent'          => 0.01,
            'daysexpireadpack'      => 90,
            'daysexpiremembership'  => 90,
            'daysvacation'          => 0,
            'image'                 => 'img/basic.png'
        ]);

        DB::table('memberships')->insert(
        [
            'id'                    => 2,
            'name'                  =>  'Premium',
            'price'                 => 450,
            'maxadpacks'            => 500,
            'refcomission'          => 0.08,
            'dailypercent'          => 0.03,
            'daysexpireadpack'      => 90,
            'daysexpiremembership'  => 90,
            'daysvacation'          => 10,
            'image'                 => 'img/premium3.png'
        ]);

        DB::table('memberships')->insert(
        [
            'id'                    => 3,
            'name'                  =>  'Ultimate',
            'price'                 => 1500,
            'maxadpacks'            => 1500,
            'refcomission'          => 0.1,
            'dailypercent'          => 0.05,
            'daysexpireadpack'      => 90,
            'daysexpiremembership'  => 180,
            'daysvacation'          => 15,
            'image'                 => 'img/ultimate.png'
        ]);

        DB::table('membership_purchase')->insert(
        [
            'id'            => 1,
            'membership_id' => 1
        ]);
        
        DB::table('wallet')->insert(
        [
            'id'            => 1,
            'btc_balance'   => 10,
            'pm_balance'    => 10,
            'stp_balance'   => 10,
            'main_balance'  => 10        
        ]);
         
        
        
        DB::table('users')->insert(
        [
            'name'      =>  'System Account',
            'email'     =>  'system@tonhits.com',
            'username'  =>  'system',
            'membership_id' => 1,
            'wallet_id'     => 1,
            'password'  =>  md5('system'),
            'pin'       =>  '1234',
            'role'      =>  3
        ]);

        DB::table('ticket_category')->insert([
            'type' => 'Soporte General',
        ]);

        DB::table('ticket_category')->insert([
            'type' => 'Quejas',
        ]);        

    }
}

