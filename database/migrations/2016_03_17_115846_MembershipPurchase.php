<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MembershipPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_purchase', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->integer('membership_id')->length(10)->unsigned();
            $table->timestamp('bought');
        });
        
        Schema::table('membership_purchase', function($table) {
            $table->foreign('membership_id')->references('id')->on('memberships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
