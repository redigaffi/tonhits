<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SupportTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->integer('user_id')->length(10)->unsigned();
            $table->integer('category')->length(10)->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamp('date');

        });

         Schema::table('support_tickets', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category')->references('id')->on('ticket_category');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
