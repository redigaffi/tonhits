<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Memberships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->string('name');
            $table->float('adpack_price');
            $table->string('color');
            $table->float('price');
            $table->string('image');
            $table->integer('min_ads_view')->default(10);
            $table->integer('maxadpacks');
            $table->float('refcomission');
            $table->float('dailypercent');
            $table->integer('daysexpireadpack');
            $table->integer('daysexpiremembership');
            $table->integer('daysvacation');
            $table->integer('credits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
