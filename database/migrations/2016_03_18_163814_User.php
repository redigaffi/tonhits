<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('username')->unique();
            $table->integer('membership_id')->length(10)->unsigned()->default(1);
            $table->string('password');
            $table->integer('pin');
            $table->integer('credits')->default(0);
            $table->integer('wallet_id')->length(10)->unsigned();
            $table->integer('ads_viewed');
            $table->timestamp('last_login')->nullable();
            $table->timestamp('datejoined')->useCurrent = true;
            $table->boolean('active')->default(true);
            $table->string('referredby')->default('system');
            $table->integer('role')->default('1');
        });

        Schema::table('users', function($table) {

            $table->foreign('referredBy')->references('username')->on('users');
            $table->foreign('membership_id')->references('id')->on('membership_purchase');
            $table->foreign('wallet_id')->references('id')->on('wallet');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
