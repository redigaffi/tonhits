<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_answers', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->integer('id_ticket')->length(10)->unsigned();
            $table->string('msg');
            $table->integer('user')->length(10)->unsigned();
            $table->timestamp('date');

        });

        Schema::table('ticket_answers', function($table) {
            $table->foreign('id_ticket')->references('id')->on('support_tickets');
            $table->foreign('user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
