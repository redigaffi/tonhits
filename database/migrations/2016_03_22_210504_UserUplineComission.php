<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserUplineComission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_upline_comission', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->integer('user_id')->length(10)->unsigned();
            $table->timestamp('date');
            $table->float('price');
            $table->float('upline');
            $table->string('type');
        });

        Schema::table('user_upline_comission', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
