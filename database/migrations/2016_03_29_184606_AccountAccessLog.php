<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountAccessLog extends Migration
{


    public function up()
    {
        Schema::create('account_access_log', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();
            $table->integer('user_id')->length(10)->unsigned();
            $table->timestamp('date');
            $table->string('ip');
            $table->string('country');
            $table->string('region');
            $table->string('city');
            $table->integer('zip');
            $table->string('lat');
            $table->string('lon');
            $table->string('isp');
        });

        Schema::table('account_access_log', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
